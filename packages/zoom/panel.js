import ZoomPanel from './src/zoom-panel'
ZoomPanel.install = function (Vue) {
  Vue.component(ZoomPanel.name, ZoomPanel)
}
export default ZoomPanel